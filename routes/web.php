<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['prefix' => 'books'], function(){
    Route::get('/', [App\Http\Controllers\BookController::class, 'index'])->name('books.index');
    Route::get('/get-data', [App\Http\Controllers\BookController::class, 'getData'])->name('books.getData');
    Route::post('/create', [App\Http\Controllers\BookController::class, 'create'])->name('books.create');
    Route::post('/update', [App\Http\Controllers\BookController::class, 'update'])->name('books.update');
    Route::get('/get-book-by-id', [App\Http\Controllers\BookController::class, 'getBookById'])->name('books.getBookById');
    // Route::get('/get-data-2', [App\Http\Controllers\BookController::class, 'getData2'])->name('books.getData2');
    Route::post('/create-stock-adjustment', [App\Http\Controllers\BookController::class, 'createStockAdjustment'])->name('books.createStockAdjustment');
    Route::get('/get-data-last-stock-card', [App\Http\Controllers\BookController::class, 'getDataLastStockCard'])->name('books.getDataLastStockCard');
});

Route::group(['prefix' => 'order'], function(){
    Route::get('/create', [App\Http\Controllers\OrderController::class, 'create'])->name('order.create');
    Route::get('/get-data-order', [App\Http\Controllers\OrderController::class, 'getDataOrder'])->name('order.getDataOrder');
    Route::post('/add-book', [App\Http\Controllers\OrderController::class, 'addBook'])->name('order.addBook');
    Route::post('/save', [App\Http\Controllers\OrderController::class, 'save'])->name('order.save');
});


Route::group(['prefix' => 'publishers'], function(){
    Route::get('/', [App\Http\Controllers\PublisherController::class, 'index'])->name('publishers.index');
    Route::get('/get-data', [App\Http\Controllers\PublisherController::class, 'getData'])->name('publishers.getData');
    Route::post('/create', [App\Http\Controllers\PublisherController::class, 'create'])->name('publishers.create');
    Route::post('/update', [App\Http\Controllers\PublisherController::class, 'update'])->name('publishers.update');
    Route::get('/get-publisher-by-id', [App\Http\Controllers\PublisherController::class, 'getPublisherById'])->name('publishers.getPublisherById');
});

Route::group(['prefix' => 'suppliers'], function(){
    Route::get('/', [App\Http\Controllers\SupplierController::class, 'index'])->name('suppliers.index');
    Route::get('/get-data', [App\Http\Controllers\SupplierController::class, 'getData'])->name('suppliers.getData');
    Route::post('/create', [App\Http\Controllers\SupplierController::class, 'create'])->name('suppliers.create');
    Route::post('/update', [App\Http\Controllers\SupplierController::class, 'update'])->name('suppliers.update');
    Route::get('/get-supplier-by-id', [App\Http\Controllers\SupplierController::class, 'getSupplierById'])->name('suppliers.getSupplierById');
});

Route::group(['prefix' => 'customers'], function(){
    Route::get('/', [App\Http\Controllers\CustomerController::class, 'index'])->name('customers.index');
    Route::get('/get-data', [App\Http\Controllers\CustomerController::class, 'getData'])->name('customers.getData');
    Route::post('/create', [App\Http\Controllers\CustomerController::class, 'create'])->name('customers.create');
    Route::post('/update', [App\Http\Controllers\CustomerController::class, 'update'])->name('customers.update');
    Route::get('/get-customer-by-id', [App\Http\Controllers\CustomerController::class, 'getCustomerById'])->name('customers.getCustomerById');
});

Route::group(['prefix' => 'sales'], function(){
    Route::get('/', [App\Http\Controllers\SaleController::class, 'index'])->name('sales.index');
    Route::get('/get-data', [App\Http\Controllers\SaleController::class, 'getData'])->name('sales.getData');
    Route::post('/pay', [App\Http\Controllers\SaleController::class, 'pay'])->name('sales.pay');
    Route::post('/cancel-order', [App\Http\Controllers\SaleController::class, 'cancelOrder'])->name('sales.cancelOrder');
    Route::get('/print-invoice', [App\Http\Controllers\SaleController::class, 'printInvoice'])->name('sales.printInvoice');
    Route::get('/get-sale-by-id', [App\Http\Controllers\SaleController::class, 'getSaleById'])->name('sales.getSaleById');
});

Route::group(['prefix' => 'purchases'], function(){
    Route::get('/', [App\Http\Controllers\PurchaseController::class, 'index'])->name('purchases.index');
    Route::get('/get-data', [App\Http\Controllers\PurchaseController::class, 'getData'])->name('purchases.getData');
    Route::get('/print-book-receipt', [App\Http\Controllers\PurchaseController::class, 'printBookReceipt'])->name('purchases.printBookReceipt');
});


Route::group(['prefix' => 'book-receipt'], function(){
    Route::get('/create', [App\Http\Controllers\BookReceiptController::class, 'create'])->name('bookReceipt.create');
    Route::get('/get-data', [App\Http\Controllers\BookReceiptController::class, 'getData'])->name('bookReceipt.getData');
    Route::post('/add-book', [App\Http\Controllers\BookReceiptController::class, 'addBook'])->name('bookReceipt.addBook');
    Route::post('/save', [App\Http\Controllers\BookReceiptController::class, 'save'])->name('bookReceipt.save');
});