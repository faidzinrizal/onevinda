<?php
namespace App\Http\Helpers;
class BaseHelper {
    public static function numberFormat($value, $decimal = 0)
    {
        return number_format($value, $decimal, ',', '.');
    }

    public static function rupiahFormat($value, $decimal=0)
    {
        return 'Rp. ' . self::numberFormat($value, $decimal);
    }

}