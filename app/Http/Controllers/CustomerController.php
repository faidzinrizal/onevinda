<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Services\Customer\CustomerDataProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

// use JeroenNoten\LaravelAdminLte\View\Components\Tool\Datatable;

class CustomerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $customerDatatable = CustomerDataProvider::getConfigDatatable();
        return view('app.master.customer.index', compact('customerDatatable'));
    }

    public function getData(Request $request)
    {
        $query = DB::table('customers');

        return DataTables::of($query)
            ->addColumn('actions', function($row) {
                $buttonGroups = "<div class='btn-group btn-group-justified'>";
                $buttonGroups .= "<button type='button' class='btn btn-info dropdown-toggle' data-toggle='dropdown' aria-expanded='false'>Aksi</button>";
                $buttonGroups .= "<div class='dropdown-menu' role='menu'>
                    <a class='dropdown-item btn-edit-customer' data-toggle='modal' data-target='#modal-customer' data-id='".$row->id."'>Ubah data</a>
                </div>";
                $buttonGroups .= "</div>";
                return $buttonGroups;
            })
            ->rawColumns(['actions'])
            ->escapeColumns()  //mencegah XSS Attack
            ->toJson();
    }

    public function getCustomerById(Request $request) {
        $customerId = $request->customer_id;

        $dataCustomer = DB::table('customers')->where('id', $customerId)->first();

        return response()->json($dataCustomer);
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'name'=>'required',
            'address'=>'required',
        ], [
            'required' => 'tidak boleh kosong'
        ]);

        $customer = new Customer();
        $customer->name = $request->name;
        $customer->address = $request->address;
        $customer->save();

        return response()->json(['customer'=>$customer]);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'name'=>'required',
            'address'=>'required',
        ], [
            'required' => ':attribute tidak boleh kosong'
        ]);


        $customer = Customer::find($request->id);
        $customer->name = $request->name;
        $customer->address = $request->address;
        $customer->save();

        return response()->json(['customer'=>$customer]);
    }
}