<?php

namespace App\Http\Controllers;

use App\Models\Supplier;
use App\Services\Supplier\SupplierDataProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

// use JeroenNoten\LaravelAdminLte\View\Components\Tool\Datatable;

class SupplierController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $supplierDatatable = SupplierDataProvider::getConfigDatatable();
        return view('app.master.supplier.index', compact('supplierDatatable'));
    }

    public function getData(Request $request)
    {
        $query = DB::table('suppliers');

        return DataTables::of($query)
            ->addColumn('actions', function($row) {
                $buttonGroups = "<div class='btn-group btn-group-justified'>";
                $buttonGroups .= "<button type='button' class='btn btn-info dropdown-toggle' data-toggle='dropdown' aria-expanded='false'>Aksi</button>";
                $buttonGroups .= "<div class='dropdown-menu' role='menu'>
                    <a class='dropdown-item btn-edit-supplier' data-toggle='modal' data-target='#modal-supplier' data-id='".$row->id."'>Ubah data</a>
                </div>";
                $buttonGroups .= "</div>";
                return $buttonGroups;
            })
            ->rawColumns(['actions'])
            ->escapeColumns()  //mencegah XSS Attack
            ->toJson();
    }

    public function getSupplierById(Request $request) {
        $supplierId = $request->supplier_id;

        $dataSupplier = DB::table('suppliers')->where('id', $supplierId)->first();

        return response()->json($dataSupplier);
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'name'=>'required',
            'address'=>'required',
        ], [
            'required' => 'tidak boleh kosong'
        ]);

        $supplier = new Supplier();
        $supplier->name = $request->name;
        $supplier->address = $request->address;
        $supplier->save();

        return response()->json(['supplier'=>$supplier]);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'name'=>'required',
            'address'=>'required',
        ], [
            'required' => ':attribute tidak boleh kosong'
        ]);


        $supplier = Supplier::find($request->id);
        $supplier->name = $request->name;
        $supplier->address = $request->address;
        $supplier->save();

        return response()->json(['supplier'=>$supplier]);
    }
}