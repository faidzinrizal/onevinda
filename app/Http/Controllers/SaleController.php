<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\Customer;
use App\Models\Lookup;
use App\Models\Sale;
use App\Services\Sales\InvoiceService;
use App\Services\Sales\OrderDataProvider;
use App\Services\Sales\SalesDataProvider;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use Barryvdh\DomPDF\Facade\Pdf;

class SaleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $saleDatatable = SalesDataProvider::getConfigDatatable();
        // $lastStockCardDatatable = LastStockCardDataProvider::getConfigDatatable();

        // $publishers = Publisher::get()->pluck('name', 'id')->toArray();
        // $adjustmentTypeList = Lookup::getDropdownList('adjustment_type');
        return view('app.transaction.sales.index', get_defined_vars());
    }


    public function getData(Request $request)
    {
        $query = DB::table('sale_v');

        return DataTables::of($query)
            ->addColumn('date_format', function($row) {
                return date('d-m-Y H:i:s', strtotime($row->date));
            })
            ->addColumn('due_date_format', function($row) {
                return date('d-m-Y', strtotime($row->due_date));
            })
            ->addColumn('actions', function($row) {
                $buttonGroups = "<div class='btn-group btn-group-justified'>";
                $buttonGroups .= "<button type='button' class='btn btn-info dropdown-toggle' data-toggle='dropdown' aria-expanded='false'>Aksi</button>";
                $buttonGroups .= "<div class='dropdown-menu' role='menu'>";
                if ($row->status != 'cancel') {
                    if ($row->status == 'unpaid') {
                        // $buttonGroups .= "<button class='btn btn-default dropdown-item btn-pay' data-id='".$row->id."'>Bayar</button>";
                        $buttonGroups .= "<a class='dropdown-item btn-payment' data-toggle='modal' data-target='#modal-payment' data-id='".$row->id."'>Pembayaran</a>";
                        $buttonGroups .= "<button class='btn btn-default dropdown-item btn-cancel-order' data-id='".$row->id."'>Batal Order</button>";
                    }
                    $buttonGroups .= "<a class='btn btn-default dropdown-item btn-print-bill' href='".Route('sales.printInvoice', ['id'=>$row->id])."' data-id='".$row->id."' target='_blank'>Cetak Invoice</a>";
                }
                $buttonGroups .= "</div>";
                $buttonGroups .= "</div>";
                return $buttonGroups;
            })
            ->rawColumns(['actions'])
            // ->addIndexColumn() //memberikan penomoran
            // ->with([
            //     "recordsTotal" => $query ? $query->count() : 0,
            //     "recordsFiltered" => $query ? $query->count() : 0,
            // ])
            ->escapeColumns()  //mencegah XSS Attack
            ->toJson();
    }

    public function getSaleById(Request $request) {
        $saleId = $request->sale_id;

        $dataSale = DB::table('sales')->where('id', $saleId)->first();

        return response()->json($dataSale);
    }

    public function pay(Request $request)
    {
        // $dataPost = $request->post();
        // $sale = Sale::find($dataPost['sale_id']);
        // $sale->status = 'paid';
        // $sale->save();
        // return $sale;

        $this->validate($request, [
            'payment_note'=>'required',
        ], [
            'required' => 'tidak boleh kosong'
        ]);


        $sale = Sale::find($request->id);
        $sale->status = 'paid';
        $sale->payment_note = $request->payment_note;
        $sale->save();

        return response()->json(['sale'=>$sale]);
    }

    public function cancelOrder(Request $request)
    {
        $dataPost = $request->post();
        $sale = Sale::find($dataPost['sale_id']);
        $sale->status = 'cancel';
        $sale->save();
        return $sale;
    }

    public function printInvoice(Request $request)
    {
        
        $id = $request->id;
        $dataInvoice = InvoiceService::getSale($id); 
        $pdf = Pdf::loadView('app.transaction.sales.print-invoice', compact('dataInvoice'))->setPaper('a4', 'landscape');
        $filename = 'Invoice-' . $dataInvoice['sale']->transaction_no;
        return $pdf->stream($filename . '.pdf', array('Attachment'=>false));
    }
}
