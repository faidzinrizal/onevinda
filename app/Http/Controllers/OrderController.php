<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\Customer;
use App\Models\Lookup;
use App\Services\Sales\OrderDataProvider;
use App\Services\Sales\OrderService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class OrderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create()
    {
        session(['sales-order'=>[]]);
        $customerList = Customer::get()->pluck('name', 'id')->toArray();
        $saleTypeList = Lookup::where('type', 'sale_type')->pluck('name', 'code')->toArray();
        $bookList = Book::get()->pluck('code_and_name', 'id')->toArray();
        
        $orderDatatable = OrderDataProvider::getConfigDatatable();
        return view('app.transaction.sales.order.create', get_defined_vars());
    }

    public function getDataOrder(Request $request)
    {
        $query = session('sales-order', []);

        return DataTables::of($query)
            ->addColumn('quantity_format', function($row) { return number_format($row['quantity'],0,",","."); })
            ->addColumn('price_format', function($row) { return 'Rp. ' . number_format($row['price'],0,",","."); })
            ->addColumn('discount_format', function($row) { return number_format($row['discount'],0,",","."); })
            ->addColumn('total_price_format', function($row) { return 'Rp. ' . number_format($row['total_price'],0,",","."); })
            ->addColumn('actions', function($row) {
                // $buttonGroups = "<div class='btn-group btn-group-justified'>";
                // $buttonGroups .= "<button type='button' class='btn btn-info dropdown-toggle' data-toggle='dropdown' aria-expanded='false'>Aksi</button>";
                // // $buttonGroups .= "<div class='dropdown-menu' role='menu'>
                // //     <a class='dropdown-item btn-edit-book' data-toggle='modal' data-target='#modal-book' data-id='".$row->id."'>Ubah Buku</a>
                // //     <a class='dropdown-item btn-adjustment-stock' data-toggle='modal' data-target='#modal-book-stock-adjustment' data-id='".$row->id."'>Penyesuaian Stok Buku</a>
                // //     <a class='dropdown-item btn-last-stock-card' data-toggle='modal' data-target='#modal-last-stock-card' data-id='".$row->id."'>5 Kartu Stok Terakhir</a>
                // // </div>";
                // $buttonGroups .= "</div>";
                return "<button class='btn btn-sm btn-danger btn-remove-order' data-id=".$row['book_id']."><i class='fa fa-trash'></i></button>";
            })
            ->rawColumns(['actions', 'quantity_format', 'price_format', 'discount_format', 'total_price_format'])
            ->escapeColumns()  //mencegah XSS Attack
            ->toJson();
    }

    public function addBook(Request $request)
    {
        $dataPost = $request->post();
        $this->validate($request, [
            'book_id'=>'required',
            'quantity'=>'required|integer',
            'discount'=>'required|integer',
        ], [
            'required' => ':attribute tidak boleh kosong',
            'integer' => ':attribute harus berupa angka'
        ]);

        // return $dataPost;
        return OrderService::addSessionBook($dataPost);
    }

    public function save(Request $request) {
        $dataPost = $request->post();
        $this->validate($request, [
            'customer_id'=>'required',
            'date'=>'required',
            'sale_type'=>'required',
            'subtotal'=>'required|integer',
            'shipping_cost'=>'required|integer',
            'total'=>'required|integer',
        ], [
            'required' => 'tidak boleh kosong',
            'integer' => 'harus berupa angka'
        ]);

        return OrderService::save($dataPost);
    }
}
