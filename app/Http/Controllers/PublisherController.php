<?php

namespace App\Http\Controllers;

use App\Models\Publisher;
use App\Services\Publisher\PublisherDataProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

// use JeroenNoten\LaravelAdminLte\View\Components\Tool\Datatable;

class PublisherController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $publisherDatatable = PublisherDataProvider::getConfigDatatable();
        $publisherStatus = [
            0=>'Penerbit',
            1=>'Penerbit Konsinyasi',
        ];
        return view('app.master.publisher.index', compact('publisherDatatable', 'publisherStatus'));
    }

    public function getData(Request $request)
    {
        $query = DB::table('publishers');

        return DataTables::of($query)
            ->addColumn('is_consignment_format', function($row) { return $row->is_consignment ? 'Penerbit Konsinyasi' : 'Penerbit'; })
            ->addColumn('actions', function($row) {
                $buttonGroups = "<div class='btn-group btn-group-justified'>";
                $buttonGroups .= "<button type='button' class='btn btn-info dropdown-toggle' data-toggle='dropdown' aria-expanded='false'>Aksi</button>";
                $buttonGroups .= "<div class='dropdown-menu' role='menu'>
                    <a class='dropdown-item btn-edit-publisher' data-toggle='modal' data-target='#modal-publisher' data-id='".$row->id."'>Ubah data</a>
                </div>";
                $buttonGroups .= "</div>";
                return $buttonGroups;
            })
            ->rawColumns(['actions', 'is_consignment_format'])
            // ->addIndexColumn() //memberikan penomoran
            // ->with([
            //     "recordsTotal" => $query ? $query->count() : 0,
            //     "recordsFiltered" => $query ? $query->count() : 0,
            // ])
            ->escapeColumns()  //mencegah XSS Attack
            ->toJson();
    }

    public function getPublisherById(Request $request) {
        $publisherId = $request->publisher_id;

        $dataPublisher = DB::table('publishers')->where('id', $publisherId)->first();

        return response()->json($dataPublisher);
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'prefix_code'=>'required',
            'name'=>'required',
            'is_consignment'=>'required',
        ], [
            'required' => 'tidak boleh kosong'
        ]);
        
        $publisher = new Publisher();
        $publisher->prefix_code = $request->prefix_code;
        $publisher->name = $request->name;
        $publisher->is_consignment = $request->is_consignment;
        $publisher->save();

        return response()->json(['publisher'=>$publisher]);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'prefix_code'=>'required',
            'name'=>'required',
            'is_consignment'=>'required',
        ], [
            'required' => ':attribute tidak boleh kosong'
        ]);


        $publisher = Publisher::find($request->id);
        $publisher->prefix_code = $request->prefix_code;
        $publisher->name = $request->name;
        $publisher->is_consignment = $request->is_consignment;
        $publisher->save();

        return response()->json(['publisher'=>$publisher]);
    }
}