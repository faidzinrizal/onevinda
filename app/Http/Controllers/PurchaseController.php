<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\Customer;
use App\Models\Lookup;
use App\Models\Sale;
use App\Services\Purchase\PurchaseDataProvider;
use App\Services\Sales\InvoiceService;
use App\Services\Sales\OrderDataProvider;
use App\Services\Sales\SalesDataProvider;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use Barryvdh\DomPDF\Facade\Pdf;

class PurchaseController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $purchaseDatatable = PurchaseDataProvider::getConfigDatatable();
        return view('app.transaction.purchase.index', get_defined_vars());
    }

    public function getData(Request $request)
    {
        $query = DB::table('purchase_v');

        return DataTables::of($query)
            ->addColumn('date_format', function($row) {
                return date('d-m-Y H:i:s', strtotime($row->date));
            })
            ->addColumn('due_date_format', function($row) {
                return $row->due_date ? date('d-m-Y', strtotime($row->due_date)) : '';
            })
            ->addColumn('actions', function($row) {
                $buttonGroups = "<div class='btn-group btn-group-justified'>";
                $buttonGroups .= "<button type='button' class='btn btn-info dropdown-toggle' data-toggle='dropdown' aria-expanded='false'>Aksi</button>";
                $buttonGroups .= "<div class='dropdown-menu' role='menu'>";
                // if ($row->status != 'cancel') {
                //     if ($row->status == 'unpaid') {
                //         // $buttonGroups .= "<button class='btn btn-default dropdown-item btn-pay' data-id='".$row->id."'>Bayar</button>";
                //         $buttonGroups .= "<a class='dropdown-item btn-payment' data-toggle='modal' data-target='#modal-payment' data-id='".$row->id."'>Pembayaran</a>";
                //         $buttonGroups .= "<button class='btn btn-default dropdown-item btn-cancel-order' data-id='".$row->id."'>Batal Order</button>";
                //     }
                    $buttonGroups .= "<a class='btn btn-default dropdown-item btn-print-book-receipt' href='".Route('purchases.printBookReceipt', ['id'=>$row->id])."' data-id='".$row->id."' target='_blank'>Cetak Pembelian</a>";
                // }
                $buttonGroups .= "</div>";
                $buttonGroups .= "</div>";
                return $buttonGroups;
            })
            ->rawColumns(['actions'])
            // ->addIndexColumn() //memberikan penomoran
            // ->with([
            //     "recordsTotal" => $query ? $query->count() : 0,
            //     "recordsFiltered" => $query ? $query->count() : 0,
            // ])
            ->escapeColumns()  //mencegah XSS Attack
            ->toJson();
    }

    public function printBookReceipt(Request $request)
    {
        $id = $request->id;
        $purchase = DB::table('purchase_v')->where('id', $id)->first();
        $purchaseDetail = DB::table('purchasedetail_v')->where('purchase_id', $id)->get();
        $dataPurchase = compact('purchase', 'purchaseDetail');
        $pdf = Pdf::loadView('app.transaction.purchase.print-book-receipt', compact('dataPurchase'))->setPaper('a4', 'landscape');
        $filename = 'book-receipt-' . $dataPurchase['purchase']->transaction_no;
        return $pdf->stream($filename . '.pdf', array('Attachment'=>false));
    }
}