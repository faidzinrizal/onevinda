<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\Lookup;
use App\Models\Publisher;
use App\Models\StockAdjustment;
use App\Models\StockCard;
use App\Services\Book\BookDataProvider;
use App\Services\Book\LastStockCardDataProvider;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

// use JeroenNoten\LaravelAdminLte\View\Components\Tool\Datatable;

class BookController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $bookDatatable = BookDataProvider::getConfigDatatable();
        $lastStockCardDatatable = LastStockCardDataProvider::getConfigDatatable();

        $publishers = Publisher::get()->pluck('name', 'id')->toArray();
        $adjustmentTypeList = Lookup::getDropdownList('adjustment_type');
        return view('app.master.book.index', compact('bookDatatable', 'publishers', 'adjustmentTypeList', 'lastStockCardDatatable'));
    }

    public function getData(Request $request)
    {
        $query = DB::table('book_v');

        return DataTables::of($query)
            ->addColumn('actions', function($row) {
                $buttonGroups = "<div class='btn-group btn-group-justified'>";
                $buttonGroups .= "<button type='button' class='btn btn-info dropdown-toggle' data-toggle='dropdown' aria-expanded='false'>Aksi</button>";
                $buttonGroups .= "<div class='dropdown-menu' role='menu'>
                    <a class='dropdown-item btn-edit-book' data-toggle='modal' data-target='#modal-book' data-id='".$row->id."'>Ubah Buku</a>
                    <a class='dropdown-item btn-adjustment-stock' data-toggle='modal' data-target='#modal-book-stock-adjustment' data-id='".$row->id."'>Penyesuaian Stok Buku</a>
                    <a class='dropdown-item btn-last-stock-card' data-toggle='modal' data-target='#modal-last-stock-card' data-id='".$row->id."'>5 Kartu Stok Terakhir</a>
                </div>";
                $buttonGroups .= "</div>";
                return $buttonGroups;
            })
            ->rawColumns(['actions'])
            // ->addIndexColumn() //memberikan penomoran
            // ->with([
            //     "recordsTotal" => $query ? $query->count() : 0,
            //     "recordsFiltered" => $query ? $query->count() : 0,
            // ])
            ->escapeColumns()  //mencegah XSS Attack
            ->toJson();
    }

    public function getBookById(Request $request) {
        $bookId = $request->book_id;

        $databook = DB::table('book_v')->where('id', $bookId)->first();

        return response()->json($databook);
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'title'=>'required',
            'publisher_id'=>'required',
        ], [
            'required' => ':attribute tidak boleh kosong'
        ]);
        
        $bookCode = Book::generateCode($request->publisher_id);
        $newBook = new Book;
        $newBook->code = $bookCode;
        $newBook->title = $request->title;
        $newBook->isbn_code = $request->isbn_code;
        $newBook->writer = $request->writer;
        $newBook->publisher_id = $request->publisher_id;
        $newBook->base_price = $request->base_price;
        $newBook->available_stock = 0; // default
        $newBook->save();

        return response()->json(['book'=>$newBook]);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'title'=>'required',
            'publisher_id'=>'required',
        ], [
            'required' => ':attribute tidak boleh kosong'
        ]);


        $book = Book::find($request->id);
        $book->title = $request->title;
        $book->isbn_code = $request->isbn_code;
        $book->writer = $request->writer;
        $book->publisher_id = $request->publisher_id;
        $book->base_price = $request->base_price;
        $book->save();

        return response()->json(['book'=>$book]);
    }

    public function createStockAdjustment(Request $request)
    {
        $this->validate($request, [
            'book_id'=>'required',
            'adjustment_type'=>'required',
            'adjustment_value'=>'required',
        ], [
            'required' => ':attribute tidak boleh kosong'
        ]);

        // create stock adjustment
        $stockAdjustment = new StockAdjustment();
        $stockAdjustment->adjustment_no = StockAdjustment::generateNumber();
        $stockAdjustment->book_id = $request->book_id;
        $stockAdjustment->adjustment_type = $request->adjustment_type;
        $stockAdjustment->adjustment_value = $request->adjustment_value;
        $stockAdjustment->note = $request->note;
        $stockAdjustment->save();

        // update available stock 
        $book = Book::find($request->book_id);
        if ($request->adjustment_type == 'stock_in') {
            $book->available_stock = $book->available_stock + $request->adjustment_value;
            $stockCard = StockCard::addStock($stockAdjustment->book_id, 'penyesuaian_stok', $stockAdjustment->adjustment_no, $stockAdjustment->adjustment_value);
        } else {
            $book->available_stock = $book->available_stock - $request->adjustment_value;
            $stockCard = StockCard::substractStock($stockAdjustment->book_id, 'penyesuaian_stok', $stockAdjustment->adjustment_no, $stockAdjustment->adjustment_value);
        }
        $book->save();

        
        return response()->json(compact('stockAdjustment', 'book', 'stockCard'));
    }

    public function getDataLastStockCard(Request $request)
    {
        $limit = 5;
        $sql = "SELECT * FROM (
            SELECT * FROM stock_cards
            WHERE book_id = :bookId 
            ORDER BY `date` DESC 
            LIMIT :limit
        ) t ORDER BY t.`date` ASC";
        $query = DB::select($sql, [':bookId' => $request->book_id, ':limit' => $limit]);
        // $query = DB::table('stock_cards')->where('book_id', $request->book_id)->limit(5)->orderBy('date', 'asc');
        return DataTables::of($query)
            ->escapeColumns()  //mencegah XSS Attack
            ->toJson();
    }
}
