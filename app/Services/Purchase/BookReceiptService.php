<?php

namespace App\Services\Purchase;

use App\Models\Book;
use App\Models\Purchase;
use App\Models\PurchaseDetail;
use App\Models\Sale;
use App\Models\SaleDetail;
use App\Services\Book\BookStockService;
use Illuminate\Support\Facades\DB;

class BookReceiptService {

    const TRANSACTION_TYPE_PURCHASE = 'pembelian';
    public static function addSessionBook($dataPost)
    {
        $book = Book::find($dataPost['book_id']);
        $dataSession = session('book-receipt');
        if (isset($dataSession[$dataPost['book_id']])) {
            $dataPost['quantity'] += $dataSession[$dataPost['book_id']]['quantity']; 
            $dataSession[$dataPost['book_id']]['quantity'] = $dataPost['quantity'];
            $dataSession[$dataPost['book_id']]['total_price'] = (($book->base_price - ($book->base_price * ($dataPost['discount'] / 100))) * $dataPost['quantity']);
        } else {
            $newData = [
                'book_id'=>$dataPost['book_id'],
                'book_code'=>$book->code,
                'book_title'=>$book->title,
                'quantity'=>$dataPost['quantity'],
                'price'=>$book->base_price,
                'discount'=>$dataPost['discount'],
                'total_price'=>(($book->base_price - ($book->base_price * ($dataPost['discount'] / 100))) * $dataPost['quantity']),
            ];
    
            $dataSession[$dataPost['book_id']] = $newData;
        }
        session(['book-receipt'=>$dataSession]);
        $sessions = session('book-receipt');

        $subtotal = 0;
        foreach ($sessions as $key => $each) {
            $subtotal += $each['total_price'];
        }

        return compact('sessions', 'subtotal');
    }

    public static function save($dataPost)
    {
        DB::beginTransaction();
        $purchase = new Purchase();
        $purchase->transaction_no = Purchase::generateNumber();
        $purchase->date = date('Y-m-d H-i-s', strtotime($dataPost['date']));
        $purchase->supplier_id = $dataPost['supplier_id'];
        $purchase->purchase_type = $dataPost['purchase_type'];
        $purchase->due_date = $dataPost['due_date'] ? date('Y-m-d', strtotime($dataPost['due_date'])) : null;
        $purchase->subtotal = $dataPost['subtotal'];
        $purchase->shipping_cost = $dataPost['shipping_cost'];
        $purchase->total = $dataPost['total'];
        $purchase->status = 'unpaid';
        
        $savedPurchase = $purchase->save();
        
        $savedPurchaseDetail = false;
        $updateStock = false;
        if ($savedPurchase) {
            $dataSessions = session('book-receipt');
            $dataReceipts = [];
            foreach ($dataSessions as $key => $each) {
                $dataReceipts[$key]['purchase_id'] = $purchase->id;
                $dataReceipts[$key]['book_id'] = $each['book_id'];
                $dataReceipts[$key]['quantity'] = $each['quantity'];
                $dataReceipts[$key]['price'] = $each['price'];
                $dataReceipts[$key]['discount'] = $each['discount'];
                $dataReceipts[$key]['total_price'] = $each['total_price'];
            }
            $savedPurchaseDetail = PurchaseDetail::insert($dataReceipts);
            // 
            if ($savedPurchaseDetail) {
                $updateStock = BookStockService::add($purchase->transaction_no, self::TRANSACTION_TYPE_PURCHASE, $dataReceipts);
                session(['book-receipt'=>[]]); //reset session
            }
        }
        if ($savedPurchase && $savedPurchaseDetail && $updateStock === true) {
            DB::commit();
        } else {
            DB::rollBack();
        }

        return compact('savedPurchase', 'savedPurchaseDetail', 'updateStock');
    }

}

