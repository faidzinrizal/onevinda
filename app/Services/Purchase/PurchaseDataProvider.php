<?php

namespace App\Services\Purchase;

class PurchaseDataProvider {

    // public static function getDataQuery($request)
    // {
    //     $query = Book::select(['code', 'title', 'isbn_code', 'writer', 'publisher_id']);
        
    //     $dataProvider = [
    //         'data' => $query->limit($request->length)->offset($request->start)->get()->toArray(),
    //         'recordsFiltered'=> $query->count(),
    //         'recordsTotal'=> $query->count(),
    //     ];
    //     return $dataProvider;
    // }

    public static function getConfigDatatable()
    {
        $heads = [
            // ['label' => 'No', 'width' => 5],
            'No. Transaksi',
            'Tanggal',
            'Supplier',
            'Cara Bayar',
            'Total Tagihan',
            'Status',
            'Jatuh Tempo',
            '',
            // ['label' => 'Actions', 'no-export' => true, 'width' => 5],
        ];
        $config = [
            'data' => [],
            'order' => [[1, 'asc']],
            'columns' => [
                ['data'=>'transaction_no'], 
                ['data'=>'date'],
                ['data'=>'supplier_name', 'orderable' => false],
                ['data'=>'purchase_type_name', 'orderable' => false],
                ['data'=>'total', 'orderable' => false],
                ['data'=>'status_name'],
                ['data'=>'due_date_format'],
                ['data'=>'actions', 'orderable' => false, 'searchable'=>false]
            ],
            'serverSide' => true,
            'pageLength' => 25,
            'ajax' => ['url'=>'/purchases/get-data']
        ];
        return compact('heads', 'config');
    }

}

