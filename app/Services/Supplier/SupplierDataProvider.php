<?php

namespace App\Services\Supplier;

use App\Models\Supplier;

class SupplierDataProvider {

    public static function getDataQuery($request)
    {
        $query = Supplier::select(['name', 'address']);
        
        $dataProvider = [
            'data' => $query->limit($request->length)->offset($request->start)->get()->toArray(),
            'recordsFiltered'=> $query->count(),
            'recordsTotal'=> $query->count(),
        ];
        return $dataProvider;
    }

    public static function getConfigDatatable()
    {
        $heads = [
            // ['label' => 'No', 'width' => 5],
            'Nama Supplier',
            'Alamat',
            '',
            // ['label' => 'Actions', 'no-export' => true, 'width' => 5],
        ];
        $config = [
            'data' => [],
            'order' => [[1, 'asc']],
            'columns' => [
                ['data'=>'name'], 
                ['data'=>'address', 'orderable' => false],
                ['data'=>'actions', 'orderable' => false, 'searchable'=>false]
            ],
            'serverSide' => true,
            'pageLength' => 25,
            'ajax' => ['url'=>'/suppliers/get-data']
        ];
        return compact('heads', 'config');
    }

}

