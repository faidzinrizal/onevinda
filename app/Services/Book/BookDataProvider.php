<?php

namespace App\Services\Book;

use App\Models\Book;

class BookDataProvider {

    public static function getDataQuery($request)
    {
        $query = Book::select(['code', 'title', 'isbn_code', 'writer', 'publisher_id']);
        
        $dataProvider = [
            'data' => $query->limit($request->length)->offset($request->start)->get()->toArray(),
            'recordsFiltered'=> $query->count(),
            'recordsTotal'=> $query->count(),
        ];
        return $dataProvider;
    }

    public static function getConfigDatatable()
    {
        $heads = [
            // ['label' => 'No', 'width' => 5],
            ['label' => 'Kode Buku', 'width' => 15],
            'Judul Buku',
            'ISBN',
            'Penulis',
            'Penerbit',
            'Harga Dasar',
            'Stok',
            '',
            // ['label' => 'Actions', 'no-export' => true, 'width' => 5],
        ];
        $config = [
            'data' => [],
            'order' => [[1, 'asc']],
            'columns' => [
                ['data'=>'code'], 
                ['data'=>'title'], 
                ['data'=>'isbn_code', 'orderable' => false],
                ['data'=>'writer', 'orderable' => false],
                ['data'=>'publisher_name', 'orderable' => false],
                ['data'=>'base_price', 'orderable' => false, 'searchable'=>false],
                ['data'=>'available_stock', 'orderable' => false, 'searchable'=>false],
                ['data'=>'actions', 'orderable' => false, 'searchable'=>false]
            ],
            'serverSide' => true,
            'pageLength' => 25,
            'ajax' => ['url'=>'/books/get-data']
        ];
        return compact('heads', 'config');
    }

}

