<?php

namespace App\Services\Book;

use App\Models\Book;

class LastStockCardDataProvider {

    public static function getDataQuery($request)
    {
        $query = Book::select(['code', 'title', 'isbn_code', 'writer', 'publisher_id']);
        
        $dataProvider = [
            'data' => $query->limit($request->length)->offset($request->start)->get()->toArray(),
            'recordsFiltered'=> $query->count(),
            'recordsTotal'=> $query->count(),
        ];
        return $dataProvider;
    }

    public static function getConfigDatatable()
    {
        $heads = [
            'Tanggal',
            'No. Transaksi',
            'Tipe Transaksi',
            'Stok Masuk',
            'Stok Keluar',
            'Sisa',
            // ['label' => 'Actions', 'no-export' => true, 'width' => 5],
        ];
        $config = [
            'data' => [],
            'order' => [[0, 'asc']],
            'columns' => [
                ['data'=>'date'], 
                ['data'=>'transaction_no', 'orderable' => false],
                ['data'=>'transaction_type', 'orderable' => false],
                ['data'=>'stock_in', 'orderable' => false],
                ['data'=>'stock_out', 'orderable' => false],
                ['data'=>'balance', 'orderable' => false],
            ],
            'serverSide' => true,
            'pageLength' => 5,
            'lengthChange' => false,
            'searching' => false,
            'paging' => false,
            'ordering' => false,
            'info' => false,
            'ajax' => ['url'=>'/books/get-data-last-stock-card']
        ];
        return compact('heads', 'config');
    }

}

