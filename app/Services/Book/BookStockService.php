<?php

namespace App\Services\Book;

use App\Models\Book;
use App\Models\StockCard;

class BookStockService {
    public static function add($trxNo, $trxType, $bookList) {
        // $bookList is array which contents at least book_id and quantity
        // insert stock card
        // update stock in books table
        try {
            //code...
            foreach ($bookList as $key => $value) {
                $prevStockCard = StockCard::where('book_id', $value['book_id'])->orderBy('id', 'DESC')->first();
                $lastBalance = $prevStockCard ? $prevStockCard->balance : 0;
                $stockCard = new StockCard();
                $stockCard->date = date('Y-m-d H:i:s');
                $stockCard->transaction_type = $trxType;
                $stockCard->transaction_no = $trxNo;
                $stockCard->book_id = $value['book_id'];
                $stockCard->stock_in = $value['quantity'];
                $stockCard->stock_out = 0;
                $stockCard->balance = $lastBalance + $value['quantity'];
                $stockCard->save();

                $book = Book::find($value['book_id']);
                $book->available_stock = $book->available_stock + $value['quantity'];
                $book->save();
            }
            return true;
        } catch (\Exception $e) {
            return $e->getMessage();
        }

    }

    public static function substract($trxNo, $trxType, $bookList) {
        // $bookList is array which contents at least book_id and quantity
        // insert stock card
        // update stock in books table
        try {
            //code...
            foreach ($bookList as $key => $value) {
                $prevStockCard = StockCard::where('book_id', $value['book_id'])->orderBy('id', 'DESC')->first();
                $lastBalance = $prevStockCard ? $prevStockCard->balance : 0;
                $stockCard = new StockCard();
                $stockCard->date = date('Y-m-d H:i:s');
                $stockCard->transaction_type = $trxType;
                $stockCard->transaction_no = $trxNo;
                $stockCard->book_id = $value['book_id'];
                $stockCard->stock_in = 0;
                $stockCard->stock_out = $value['quantity'];
                $stockCard->balance = $lastBalance - $value['quantity'];
                $stockCard->save();

                $book = Book::find($value['book_id']);
                $book->available_stock = $book->available_stock - $value['quantity'];
                $book->save();
            }
            return true;
        } catch (\Exception $e) {
            return $e->getMessage();
        }

    }
}