<?php

namespace App\Services\Sales;

class OrderDataProvider {

    public static function getConfigDatatable()
    {
        $heads = [
            // ['label' => 'No', 'width' => 5],
            'Kode Buku',
            'Judul Buku',
            'Jumlah Beli',
            'Harga Satuan',
            'Diskon (%)',
            'Subtotal',
            '',
            // ['label' => 'Actions', 'no-export' => true, 'width' => 5],
        ];
        $config = [
            'data' => [],
            'ordering' => false,
            'searching' => false,
            'paging' => false,
            'bInfo' => false,
            'columns' => [
                ['data'=>'book_code', 'width'=>'10%'], 
                ['data'=>'book_title', 'width'=>'45%'], 
                ['data'=>'quantity_format', 'width'=>'10%'], 
                ['data'=>'price_format', 'width'=>'10%'], 
                ['data'=>'discount_format', 'width'=>'10%'], 
                ['data'=>'total_price_format', 'width'=>'10%'], 
                ['data'=>'actions', 'width'=>'5%'],
                ['data'=>'book_id', 'visible'=>false],
            ],
            'serverSide' => true,
            // 'pageLength' => 25,
            'ajax' => ['url'=>'/order/get-data-order'],
        ];
        return compact('heads', 'config');
    }

}

