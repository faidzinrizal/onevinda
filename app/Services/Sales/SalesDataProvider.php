<?php

namespace App\Services\Sales;

class SalesDataProvider {

    // public static function getDataQuery($request)
    // {
    //     $query = Book::select(['code', 'title', 'isbn_code', 'writer', 'publisher_id']);
        
    //     $dataProvider = [
    //         'data' => $query->limit($request->length)->offset($request->start)->get()->toArray(),
    //         'recordsFiltered'=> $query->count(),
    //         'recordsTotal'=> $query->count(),
    //     ];
    //     return $dataProvider;
    // }

    public static function getConfigDatatable()
    {
        $heads = [
            // ['label' => 'No', 'width' => 5],
            'No. Transaksi',
            'Tanggal',
            'Reseller',
            'Cara Bayar',
            'Total Tagihan',
            'Jatuh Tempo',
            'Status',
            '',
            // ['label' => 'Actions', 'no-export' => true, 'width' => 5],
        ];
        $config = [
            'data' => [],
            'order' => [[1, 'asc']],
            'columns' => [
                ['data'=>'transaction_no'], 
                ['data'=>'date'],
                ['data'=>'customer_name', 'orderable' => false],
                ['data'=>'sale_type_name', 'orderable' => false],
                ['data'=>'total', 'orderable' => false],
                ['data'=>'due_date'],
                ['data'=>'status_name'],
                ['data'=>'actions', 'orderable' => false, 'searchable'=>false]
            ],
            'serverSide' => true,
            'pageLength' => 25,
            'ajax' => ['url'=>'/sales/get-data']
        ];
        return compact('heads', 'config');
    }

}

