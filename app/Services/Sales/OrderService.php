<?php

namespace App\Services\Sales;

use App\Models\Book;
use App\Models\Sale;
use App\Models\SaleDetail;
use App\Services\Book\BookStockService;
use Illuminate\Support\Facades\DB;

class OrderService {

    const TRANSACTION_TYPE_SALE = 'penjualan';
    public static function addSessionBook($dataPost)
    {
        $book = Book::find($dataPost['book_id']);
        $dataSession = session('sales-order');
        if (isset($dataSession[$dataPost['book_id']])) {
            $dataPost['quantity'] += $dataSession[$dataPost['book_id']]['quantity']; 
            $dataSession[$dataPost['book_id']]['quantity'] = $dataPost['quantity'];
            $dataSession[$dataPost['book_id']]['total_price'] = (($book->base_price - ($book->base_price * ($dataPost['discount'] / 100))) * $dataPost['quantity']);
        } else {
            $newData = [
                'book_id'=>$dataPost['book_id'],
                'book_code'=>$book->code,
                'book_title'=>$book->title,
                'quantity'=>$dataPost['quantity'],
                'price'=>$book->base_price,
                'discount'=>$dataPost['discount'],
                'total_price'=>(($book->base_price - ($book->base_price * ($dataPost['discount'] / 100))) * $dataPost['quantity']),
            ];
    
            $dataSession[$dataPost['book_id']] = $newData;
        }
        session(['sales-order'=>$dataSession]);
        $sessions = session('sales-order');

        $subtotal = 0;
        foreach ($sessions as $key => $each) {
            $subtotal += $each['total_price'];
        }

        return compact('sessions', 'subtotal');
    }

    public static function save($dataPost)
    {
        DB::beginTransaction();
        $sale = new Sale();
        $sale->transaction_no = Sale::generateNumber();
        $sale->date = date('Y-m-d H-i-s', strtotime($dataPost['date']));
        $sale->customer_id = $dataPost['customer_id'];
        $sale->sale_type = $dataPost['sale_type'];
        $sale->due_date = $dataPost['due_date'] ? date('Y-m-d', strtotime($dataPost['due_date'])) : null;
        $sale->subtotal = $dataPost['subtotal'];
        $sale->shipping_cost = $dataPost['shipping_cost'];
        $sale->total = $dataPost['total'];
        $sale->status = 'unpaid';
        
        $savedSale = $sale->save();
        
        $savedSaleDetail = false;
        $updateStock = false;
        if ($savedSale) {
            $dataSessions = session('sales-order');
            $dataOrders = [];
            foreach ($dataSessions as $key => $each) {
                $dataOrders[$key]['sale_id'] = $sale->id;
                $dataOrders[$key]['book_id'] = $each['book_id'];
                $dataOrders[$key]['quantity'] = $each['quantity'];
                $dataOrders[$key]['price'] = $each['price'];
                $dataOrders[$key]['discount'] = $each['discount'];
                $dataOrders[$key]['total_price'] = $each['total_price'];
            }
            $savedSaleDetail = SaleDetail::insert($dataOrders);
            // 
            if ($savedSaleDetail) {
                $updateStock = BookStockService::substract($sale->transaction_no, self::TRANSACTION_TYPE_SALE, $dataOrders);
                session(['sales-order'=>[]]); //reset session
            }
        }
        if ($savedSale && $savedSaleDetail && $updateStock === true) {
            DB::commit();
        } else {
            DB::rollBack();
        }

        return compact('savedSale', 'savedSaleDetail', 'updateStock');
    }

}

