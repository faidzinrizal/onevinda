<?php

namespace App\Services\Sales;

use App\Models\Book;
use App\Models\Sale;
use App\Models\SaleDetail;
use Illuminate\Support\Facades\DB;

class InvoiceService {
    public static function getSale($id)
    {
        $sale = DB::table('sale_v')->where('id', $id)->first();
        $saleDetail = DB::table('saledetail_v')->where('sale_id', $id)->get();
        return compact('sale', 'saleDetail');
    }
}