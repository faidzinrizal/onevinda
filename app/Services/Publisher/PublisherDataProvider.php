<?php

namespace App\Services\Publisher;

use App\Models\Publisher;

class PublisherDataProvider {

    public static function getDataQuery($request)
    {
        $query = Publisher::select(['prefix_code', 'name', 'is_consignment']);
        
        $dataProvider = [
            'data' => $query->limit($request->length)->offset($request->start)->get()->toArray(),
            'recordsFiltered'=> $query->count(),
            'recordsTotal'=> $query->count(),
        ];
        return $dataProvider;
    }

    public static function getConfigDatatable()
    {
        $heads = [
            // ['label' => 'No', 'width' => 5],
            ['label' => 'Kode Penerbit', 'width' => 15],
            'Nama Penerbit',
            'Status Penerbit',
            '',
            // ['label' => 'Actions', 'no-export' => true, 'width' => 5],
        ];
        $config = [
            'data' => [],
            'order' => [[1, 'asc']],
            'columns' => [
                ['data'=>'prefix_code'], 
                ['data'=>'name'], 
                ['data'=>'is_consignment_format', 'orderable' => false],
                ['data'=>'actions', 'orderable' => false, 'searchable'=>false]
            ],
            'serverSide' => true,
            'pageLength' => 25,
            'ajax' => ['url'=>'/publishers/get-data']
        ];
        return compact('heads', 'config');
    }

}

