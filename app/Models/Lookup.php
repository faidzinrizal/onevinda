<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lookup extends Model
{
    use HasFactory;
    
    protected $table = 'lookups';

    public static function getDropdownList($type)
    {
        return self::where('type', $type)->pluck('name', 'code')->toArray();
    }
}
