<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StockCard extends Model
{
    use HasFactory;

    public static function addStock($bookId, $trxType, $trxNo='', $value) {
        // find previous stock card
        $previousStockCard = StockCard::where('book_id', $bookId)->orderBy('id', 'desc')->limit(1)->first();
        $previousStockCardBalance = $previousStockCard ? $previousStockCard->balance : 0;
        
        // create stock card
        $stockCard = new StockCard();
        $stockCard->date = date('Y-m-d H:i:s');
        $stockCard->book_id = $bookId;
        $stockCard->transaction_type = $trxType;
        $stockCard->transaction_no = $trxNo;
        $stockCard->stock_in = $value;
        $stockCard->balance = $previousStockCardBalance + $value;
        $saved = $stockCard->save();
        return $saved ? $stockCard : false;
    }

    public static function substractStock($bookId, $trxType, $trxNo='', $value) {
        // find previous stock card
        $previousStockCard = StockCard::where('book_id', $bookId)->orderBy('id', 'desc')->limit(1)->first();
        $previousStockCardBalance = $previousStockCard ? $previousStockCard->balance : 0;
        
        // create stock card
        $stockCard = new StockCard();
        $stockCard->date = date('Y-m-d H:i:s');
        $stockCard->book_id = $bookId;
        $stockCard->transaction_type = $trxType;
        $stockCard->transaction_no = $trxNo;
        $stockCard->stock_out = $value;
        $stockCard->balance = $previousStockCardBalance - $value;
        $saved = $stockCard->save();
        return $saved ? $stockCard : false;
    }
}
