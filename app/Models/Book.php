<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;
    
    protected $table = 'books';

    public function getCodeAndNameAttribute()
    {
        return $this->code . ' - ' . $this->title;
    }
    public static function generateCode($publisherId)
    {
        $lastBookByPublisher = Self::where('publisher_id', $publisherId)->orderBy('code', 'DESC')->first();
        $lastCode = $lastBookByPublisher ? $lastBookByPublisher->code : null;

        if ($lastCode) {
            $parsedCode = explode('.', $lastCode);
            $newNumber = sprintf("%04s", $parsedCode[1] + 1);
            $generatedCode = $parsedCode[0] . '.' . $newNumber;
        } else {
            $publisher = Publisher::find($publisherId);
            $publisherCode = $publisher->prefix_code;
            $generatedCode = $publisherCode . '.' . '0001'; 
        }
        return $generatedCode;
    }
}
