<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    use HasFactory;
    protected $table = 'sales';

    public static function generateNumber()
    {
        $pattern = 'SAL-YYMM0000';
        $draftPattern = str_replace('0000', '', $pattern);
        $prefix = substr($pattern, 0, 4);

        $lastData = self::orderBy('id', 'desc')->limit(1)->first();
        $isNewCounter = false;
        $lastCounter = null;
        if ($lastData) {
            $lastCounter = $lastData->transaction_no;
        } else {
            $isNewCounter = true;
        }


        $prefixLength = $prefix ? strlen($prefix) : 0;

        if ($lastCounter) {
            $checker = substr($lastCounter, $prefixLength, 4);
            $dateChecker = date('ym');
            $isNewCounter = $checker == $dateChecker ? false : true;
        } else {
            $isNewCounter = true;
        }
        
        $draftPattern = self::parseYear($draftPattern);
        $draftPattern = self::parseMonth($draftPattern);
        $draftPattern = self::parseDay($draftPattern);

        $checkLast = $isNewCounter == false ? substr($lastCounter, -4) : '0000';
        
        $nextNumber = sprintf("%04s", $checkLast + 1);

        return $draftPattern . $nextNumber;
    }

    private static function parseYear($pattern) {
        if (str_contains($pattern, 'YY')) { 
            return str_replace('YY', date('y'), $pattern);
        }
        return $pattern;
    }
    private static function parseMonth($pattern) {
        if (str_contains($pattern, 'MM')) { 
            return str_replace('MM', date('m'), $pattern);
        }
        return $pattern;
    }
    private static function parseDay($pattern) {
        if (str_contains($pattern, 'DD')) { 
            return str_replace('DD', date('d'), $pattern);
        }
        return $pattern;
    }
}
