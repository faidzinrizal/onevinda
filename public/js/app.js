var helpers = {
    listen: false,
    options: {},
    onClick: function (object) {
      var _obj = $(object);
    },
    ajax: function (options, _this, btn) {
      $.ajax({
        url: options.url,
        type: options.method,
        dataType: options.dataType,
        async: options.async,
        contentType: options.contentType,
        processData: options.processData,
        data: options.data,
        beforeSend: function (xhr) {
          $('.form-control').removeClass('is-invalid');
          $('.invalid-feedback').remove();
        },
        success: function (data) {
            var data = data;
            options.success(data);
            if (data.notif == true) {
              notif({
                msg: data.message,
                type: data.type,
              });
            }
            Swal.close()
        },
        error: function (data, status, error) {
          try {
            var error = data.responseJSON.response;
            var sttsErr = data.status;
            var dataError = data.responseJSON.errors;
            if (sttsErr == '422') {
                // Parsing Error;
                if (dataError instanceof Array || dataError instanceof Object) {
                  $.each(dataError, function (key, val) {
                    var _ids = $('#' + key + '');
                    _ids.addClass('is-invalid');
                    var _select2 = _ids.closest('div').find('.select2-container');
                    var _multiselect = _ids.closest('div').find('.ms-parent');
                    var _checkbox = $('#last-checkbox-' + key + '');
  
                    var _multiselectIds = '';
                    if (_multiselect.length) {
                       _multiselectIds = $('.ms-parent').addClass('ms-parent-'+ key)
                    }
  
                    if (_multiselectIds.length) {
                        _multiselectIds.after(
                          '<div class="invalid-feedback">' +
                          val[0] +
                          '</div>'
                        );
                    }else if (_select2.length) {
                        _select2.after(
                          '<div class="invalid-feedback">' +
                          val[0] +
                          '</div>'
                        );                      
                    }else if (_checkbox.length) {
                      _checkbox.after(
                        '<div class="invalid-feedback">' +
                        val[0] +
                        '</div>'
                      );
                    } else{
                      _ids.after(
                        '<div class="invalid-feedback">' +
                        val[0] +
                        '</div>'
                      );
                    }
                  });            
                }else{
                //   notif({
                //     msg: dataError,
                //     type: "warning"
                //   });
                }
              } 
            //   if ($('#deleteModal').hasClass('show')) {
            //     $('#deleteModal').modal('hide');
            //   }
              
            //   if (options.confirm) {
            //     Swal.close()
            //     notif({
            //       msg: 'Mohon Periksa Kembali Inputan!',
            //       type: 'warning',
            //     });
            //   }
  
  
            } catch ($e) {
  
            }     
        },
      }).done(function () {
  
      });
    },
  };
  
  
  $.fn.helperForm = function (trigger, content) {
    var _this = $(this);
    var _default = {
      url: typeof _this.attr('action') !== 'undefined' ? _this.attr('action') : null,
      method:typeof _this.attr('method') !== 'undefined'? _this.attr('method'): 'POST',
      data: {},
      async: true,
      formInput: _this,
      success: function (event, data) { },
      error: function (event, data) { },
      before: function (event) { },
      dataType: 'json',
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      processData: true,
      confirm_alert: true,
    };
    var options = $.extend({}, _default, content);
    var _confirm = typeof options.confirm !== 'undefined'? options.confirm : false;
    var _confirmTitle = typeof options.confirm_title !== 'undefined'? options.confirm_title: 'Apakah anda yakin ?';
    var _confirmMessage = typeof options.confirm_message !== 'undefined'? options.confirm_message: '';
    var _confirmIcon = typeof options.confirm_icon !== 'undefined'? options.confirm_icon: '';
    var _confirmType = typeof options.confirm_type !== 'undefined'? options.confirm_type: '';
    if (_confirm) {
      Swal.fire({
        title: _confirmTitle,
        text: _confirmMessage,
        // reverseButtons: true,
        showCancelButton: true,
        confirmButtonText: 'Ya',
        cancelButtonText: "Tidak",
        type: _confirmType,
      }).then((result) => {
        if (result) {
          sendToAjax(trigger, options, _this)
        }
      });

        // function(isConfirm) {
        //   console.log('masuk sini');
        // if (isConfirm) {
        //    return
        //   sendToAjax(trigger, options, _this)
        // } else {
        //   Swal.close()
        //   return false;
        // }
    }else{
      sendToAjax(trigger, options, _this)
    }
  };
  
  function sendToAjax(trigger, options, _this) {
    switch (trigger) {
      case 'click':
        helpers.ajax(options, _this, _this);
        break;
      default:
        helpers.options = options;
        $(this).submit(function (event) {
            event.preventDefault();
            helpers.ajax(
              helpers.options,
              _this,
              _this.find('button[type=submit]')
            );
        });
        break;
    }
  }

  const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    // timerProgressBar: true,
    // didOpen: (toast) => {
    //   toast.addEventListener('mouseenter', Swal.stopTimer)
    //   toast.addEventListener('mouseleave', Swal.resumeTimer)
    // }
  })