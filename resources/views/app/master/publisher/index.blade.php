
@extends('adminlte::page')

@section('title', 'Kelola Penerbit')

@section('content_header')
    <h1>Kelola Penerbit</h1>
    <div class="btn-group">
    {{-- Example button to open modal --}}
        <x-adminlte-button label="Tambah penerbit" data-toggle="modal" data-target="#modal-publisher" class="btn-success btn-sm btn-create"/>
    </div>
@stop

@section('content')
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

    <x-adminlte-datatable id="publisher-table" :heads="$publisherDatatable['heads']" head-theme="dark" :config="$publisherDatatable['config']"
    striped hoverable bordered compressed/>


    {{-- Modal Create / Edit --}}
    <x-adminlte-modal id="modal-publisher" title="Tambah Penerbit" size="lg">
        <form id="publisher-form">
            <div class="row">
                @csrf
                <input type="hidden" id="id" name="id" value=""> 
                <x-adminlte-input 
                    name="prefix_code" 
                    label="Kode Penerbit" 
                    placeholder="Kode Penerbit (3 Huruf)"
                    fgroup-class="col-md-12" 
                    value=""
                    disable-feedback="true"
                    />
                <x-adminlte-input 
                    name="name" 
                    label="Nama Penerbit" 
                    placeholder="Nama Penerbit"
                    fgroup-class="col-md-12" 
                    disable-feedback="true"/>
                <x-adminlte-select2 
                    name="is_consignment"
                    label="Status Penerbit"
                    placeholder="Status Penerbit"
                    fgroup-class="col-md-12"
                    disable-feedback="true">
                    <x-adminlte-options :options=$publisherStatus empty-option="--Pilih--"/>
                </x-adminlte-select2>
            </div>
        </form>

        <x-slot name="footerSlot">
            <x-adminlte-button class="mr-auto btn-save" theme="success" label="Simpan"/>
            <x-adminlte-button class="mr-auto btn-update" theme="success" label="Ubah" style="display:none; "/>
            <x-adminlte-button theme="default" label="Batal" data-dismiss="modal"/>
        </x-slot>
    </x-adminlte-modal>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script type="text/javascript">
    $(document).ready(function() {
        $(".btn-save").click((e)=> {
            e.preventDefault()
            // return;

            $(this).helperForm('click',{
                url: '/publishers/create',
                data: $('#publisher-form').serializeArray(),
                confirm: true,
                confirm_type: 'warning', // warning, error, info
                // confirm_title: 'Apakah bla bla?',
                // confirm_message: 'bla bla bla bla',
                success : function(res) {
                    $('.modal').modal('hide');
                    setTimeout(()=>{
                        Toast.fire({
                            icon: 'success',
                            title: 'Data berhasil disimpan'
                        });
                    }, 1000);
                }
            });
        });
    });    

    $(document).on('click', '.btn-edit-publisher', (e)=>{
        e.preventDefault();
        var _this = e.target;
        var publisherId = _this.dataset.id;

        $.ajax({
            url: '/publishers/get-publisher-by-id',
            type: 'GET',
            data: {publisher_id:publisherId},
            beforeSend: function (xhr) {

            },
            success: function (data) {
                console.log(data)
                $('#publisher-form').find('#id').val(data.id);
                $('#publisher-form').find('#prefix_code').val(data.prefix_code);
                $('#publisher-form').find('#name').val(data.name);
                $('#publisher-form').find('#is_consignment').val(data.is_consignment).change();
            }
        });

        $('#modal-publisher').find('.modal-title').text('Ubah Data Penerbit');
        $('.btn-save').hide();
        $('.btn-update').show();
    })

    $(".btn-update").click((e)=> {
        e.preventDefault()
        // return;

        $(this).helperForm('click',{
            url: '/publishers/update',
            data: $('#publisher-form').serializeArray(),
            confirm: true,
            confirm_type: 'warning', // warning, error, info
            // confirm_title: 'Apakah bla bla?',
            // confirm_message: 'bla bla bla bla',
            success : function(res) {
                $('.modal').modal('hide');
                setTimeout(()=>{
                    Toast.fire({
                        icon: 'success',
                        title: 'Data berhasil diubah'
                    });
                }, 1000);
            }
        });
    });
</script>
@stop