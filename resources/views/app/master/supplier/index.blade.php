
@extends('adminlte::page')

@section('title', 'Kelola Supplier')

@section('content_header')
    <h1>Kelola Supplier</h1>
    <div class="btn-group">
    {{-- Example button to open modal --}}
        <x-adminlte-button label="Tambah Supplier" data-toggle="modal" data-target="#modal-supplier" class="btn-success btn-sm btn-create"/>
    </div>
@stop

@section('content')
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

    <x-adminlte-datatable id="supplier-table" :heads="$supplierDatatable['heads']" head-theme="dark" :config="$supplierDatatable['config']"
    striped hoverable bordered compressed/>


    {{-- Modal Create / Edit --}}
    <x-adminlte-modal id="modal-supplier" title="Tambah Supplier" size="lg">
        <form id="supplier-form">
            <div class="row">
                @csrf
                <input type="hidden" id="id" name="id" value=""> 
                <x-adminlte-input 
                    name="name" 
                    label="Nama Supplier" 
                    placeholder="Nama Supplier"
                    fgroup-class="col-md-12" 
                    disable-feedback="true"/>
                <x-adminlte-input 
                    name="address" 
                    label="Alamat" 
                    placeholder="Alamat"
                    fgroup-class="col-md-12" 
                    value=""
                    disable-feedback="true"
                    />
            </div>
        </form>

        <x-slot name="footerSlot">
            <x-adminlte-button class="mr-auto btn-save" theme="success" label="Simpan"/>
            <x-adminlte-button class="mr-auto btn-update" theme="success" label="Ubah" style="display:none; "/>
            <x-adminlte-button theme="default" label="Batal" data-dismiss="modal"/>
        </x-slot>
    </x-adminlte-modal>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script type="text/javascript">
    $(document).ready(function() {
        $(".btn-save").click((e)=> {
            e.preventDefault()
            // return;

            $(this).helperForm('click',{
                url: '/suppliers/create',
                data: $('#supplier-form').serializeArray(),
                confirm: true,
                confirm_type: 'warning', // warning, error, info
                // confirm_title: 'Apakah bla bla?',
                // confirm_message: 'bla bla bla bla',
                success : function(res) {
                    $('.modal').modal('hide');
                    setTimeout(()=>{
                        Toast.fire({
                            icon: 'success',
                            title: 'Data berhasil disimpan'
                        });
                    }, 1000);
                }
            });
        });
    });    

    $(document).on('click', '.btn-edit-supplier', (e)=>{
        e.preventDefault();
        var _this = e.target;
        var supplierId = _this.dataset.id;

        $.ajax({
            url: '/suppliers/get-supplier-by-id',
            type: 'GET',
            data: {supplier_id:supplierId},
            beforeSend: function (xhr) {

            },
            success: function (data) {
                console.log(data)
                $('#supplier-form').find('#id').val(data.id);
                $('#supplier-form').find('#name').val(data.name);
                $('#supplier-form').find('#address').val(data.address);
            }
        });

        $('#modal-supplier').find('.modal-title').text('Ubah Data Supplier');
        $('.btn-save').hide();
        $('.btn-update').show();
    })

    $(".btn-update").click((e)=> {
        e.preventDefault()
        // return;

        $(this).helperForm('click',{
            url: '/suppliers/update',
            data: $('#supplier-form').serializeArray(),
            confirm: true,
            confirm_type: 'warning', // warning, error, info
            // confirm_title: 'Apakah bla bla?',
            // confirm_message: 'bla bla bla bla',
            success : function(res) {
                $('.modal').modal('hide');
                setTimeout(()=>{
                    Toast.fire({
                        icon: 'success',
                        title: 'Data berhasil diubah'
                    });
                }, 1000);
            }
        });
    });
</script>
@stop