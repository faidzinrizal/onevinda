
@extends('adminlte::page')

@section('title', 'Kelola Reseller')

@section('content_header')
    <h1>Kelola Reseller</h1>
    <div class="btn-group">
    {{-- Example button to open modal --}}
        <x-adminlte-button label="Tambah Reseller" data-toggle="modal" data-target="#modal-customer" class="btn-success btn-sm btn-create"/>
    </div>
@stop

@section('content')
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

    <x-adminlte-datatable id="customer-table" :heads="$customerDatatable['heads']" head-theme="dark" :config="$customerDatatable['config']"
    striped hoverable bordered compressed/>


    {{-- Modal Create / Edit --}}
    <x-adminlte-modal id="modal-customer" title="Tambah Reseller" size="lg">
        <form id="customer-form">
            <div class="row">
                @csrf
                <input type="hidden" id="id" name="id" value=""> 
                <x-adminlte-input 
                    name="name" 
                    label="Nama Reseller" 
                    placeholder="Nama Reseller"
                    fgroup-class="col-md-12" 
                    disable-feedback="true"/>
                <x-adminlte-input 
                    name="address" 
                    label="Alamat" 
                    placeholder="Alamat"
                    fgroup-class="col-md-12" 
                    value=""
                    disable-feedback="true"
                    />
            </div>
        </form>

        <x-slot name="footerSlot">
            <x-adminlte-button class="mr-auto btn-save" theme="success" label="Simpan"/>
            <x-adminlte-button class="mr-auto btn-update" theme="success" label="Ubah" style="display:none; "/>
            <x-adminlte-button theme="default" label="Batal" data-dismiss="modal"/>
        </x-slot>
    </x-adminlte-modal>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script type="text/javascript">
    $(document).ready(function() {
        $(".btn-save").click((e)=> {
            e.preventDefault()
            // return;

            $(this).helperForm('click',{
                url: '/customers/create',
                data: $('#customer-form').serializeArray(),
                confirm: true,
                confirm_type: 'warning', // warning, error, info
                // confirm_title: 'Apakah bla bla?',
                // confirm_message: 'bla bla bla bla',
                success : function(res) {
                    $('.modal').modal('hide');
                    setTimeout(()=>{
                        Toast.fire({
                            icon: 'success',
                            title: 'Data berhasil disimpan'
                        });
                    }, 1000);
                }
            });
        });
    });    

    $(document).on('click', '.btn-edit-customer', (e)=>{
        e.preventDefault();
        var _this = e.target;
        var customerId = _this.dataset.id;

        $.ajax({
            url: '/customers/get-customer-by-id',
            type: 'GET',
            data: {customer_id:customerId},
            beforeSend: function (xhr) {

            },
            success: function (data) {
                console.log(data)
                $('#customer-form').find('#id').val(data.id);
                $('#customer-form').find('#name').val(data.name);
                $('#customer-form').find('#address').val(data.address);
            }
        });

        $('#modal-customer').find('.modal-title').text('Ubah Data Reseller');
        $('.btn-save').hide();
        $('.btn-update').show();
    })

    $(".btn-update").click((e)=> {
        e.preventDefault()
        // return;

        $(this).helperForm('click',{
            url: '/customers/update',
            data: $('#customer-form').serializeArray(),
            confirm: true,
            confirm_type: 'warning', // warning, error, info
            // confirm_title: 'Apakah bla bla?',
            // confirm_message: 'bla bla bla bla',
            success : function(res) {
                $('.modal').modal('hide');
                setTimeout(()=>{
                    Toast.fire({
                        icon: 'success',
                        title: 'Data berhasil diubah'
                    });
                }, 1000);
            }
        });
    });
</script>
@stop