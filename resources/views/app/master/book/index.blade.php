
@extends('adminlte::page')

@section('title', 'Kelola Buku')

@section('content_header')
    <h1>Kelola Buku</h1>
    <div class="btn-group">
    {{-- Example button to open modal --}}
        <x-adminlte-button label="Tambah Buku" data-toggle="modal" data-target="#modal-book" class="btn-success btn-sm btn-create-book"/>
    </div>
@stop

@section('content')
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

    <x-adminlte-datatable id="book-table" :heads="$bookDatatable['heads']" head-theme="dark" :config="$bookDatatable['config']"
    striped hoverable bordered compressed/>


    {{-- Modal Create / Edit --}}
    <x-adminlte-modal id="modal-book" title="Tambah Buku" size="lg">
        <form id="book-form">
            <div class="row">
                @csrf
                <input type="hidden" id="id" name="id" value=""> 
                <x-adminlte-input 
                    name="code" 
                    label="Kode Buku" 
                    placeholder="Kode Buku (TIDAK PERLU DIISI)"
                    fgroup-class="col-md-12" 
                    value=""
                    disabled
                    disable-feedback="true"
                    />
                <x-adminlte-input 
                    name="title" 
                    label="Judul Buku" 
                    placeholder="Judul Buku"
                    fgroup-class="col-md-12" 
                    disable-feedback="true"/>
                <x-adminlte-input 
                    name="isbn_code" 
                    label="Kode ISBN" 
                    placeholder="Kode ISBN"
                    fgroup-class="col-md-12" 
                    
                    disable-feedback="true"/>
                <x-adminlte-input 
                    name="writer" 
                    label="Penulis" 
                    placeholder="Penulis"
                    fgroup-class="col-md-12" 
                    disable-feedback="true"/>
                <x-adminlte-input 
                    name="base_price" 
                    label="Harga Dasar" 
                    placeholder="Harga Dasar"
                    fgroup-class="col-md-12" 
                    disable-feedback="true"/>
                <x-adminlte-select2 name="publisher_id"
                    label="Penerbit"
                    placeholder="Penerbit"
                    fgroup-class="col-md-12"
                    disable-feedback="true">
                    <x-adminlte-options :options=$publishers empty-option="--Pilih--"/>
                </x-adminlte-select2>
            </div>
        </form>

        <x-slot name="footerSlot">
            <x-adminlte-button class="mr-auto btn-save" theme="success" label="Simpan"/>
            <x-adminlte-button class="mr-auto btn-update" theme="success" label="Ubah" style="display:none; "/>
            <x-adminlte-button theme="default" label="Batal" data-dismiss="modal"/>
        </x-slot>
    </x-adminlte-modal>

    {{-- Modal update stock --}}
    <x-adminlte-modal id="modal-book-stock-adjustment" title="Penyesuaian Stok Buku" size="lg">
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4">
                        <label for="info-book_code">Kode Buku</label>
                    </div>
                    <div class="col-md-8">
                        <div class="info-book_code">-</div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label for="info-book_publisher">Penerbit</label>
                    </div>
                    <div class="col-md-8">
                        <div class="info-book_publisher">-</div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4">
                        <label for="info-book_title">Judul Buku</label>
                    </div>
                    <div class="col-md-8">
                        <div class="info-book_title">-</div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label for="info-book_available_stock">Stok Tersedia</label>
                    </div>
                    <div class="col-md-8">
                        <div class="info-book_available_stock">0</div>
                    </div>
                </div>
            </div>
        </div>
        <hr>
            
        <form id="stock-adjustment-form">
            <div class="row">
                @csrf
                <input type="hidden" id="book_id" name="book_id" value="">
                <div class="col-md-6">
                    <x-adminlte-select2 name="adjustment_type"
                        label="Tipe Penyesuaian"
                        fgroup-class="col-md-12"
                        class="calculate"
                        >
                        <x-adminlte-options :options=$adjustmentTypeList empty-option="--Pilih--"/>
                    </x-adminlte-select2>
                    
                    <x-adminlte-textarea name="note" label="Catatan" placeholder="Catatan"/>
                </div>
                <div class="col-md-6">
                <input type="hidden" id="available_stock" name="available_stock" value="0">
                    <x-adminlte-input 
                        type="number" 
                        name="adjustment_value" 
                        label="Penyesuaian Stok"
                        placeholder="Penyesuaian Stok"
                        value="0"
                        class="calculate"
                        fgroup-class="col-md-12"
                        disable-feedback="true"/>

                    <x-adminlte-input 
                        name="final_stock" 
                        label="Stok Akhir"
                        placeholder="Stok Akhir"
                        value="0"
                        fgroup-class="col-md-12"
                        disabled
                        disable-feedback="true"/>
                </div>
                
            </div>
        </form>

        <x-slot name="footerSlot">
            <x-adminlte-button class="mr-auto btn-save-adjustment" theme="success" label="Simpan Penyesuaian Stok"/>
            <x-adminlte-button theme="default" label="Batal" data-dismiss="modal"/>
        </x-slot>
    </x-adminlte-modal>

    {{-- Modal stock card --}}
    <x-adminlte-modal id="modal-last-stock-card" title="5 Kartu Stok Terakhir" size="lg">
    <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4">
                        <label for="info-book_code">Kode Buku</label>
                    </div>
                    <div class="col-md-8">
                        <div class="info-book_code">-</div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label for="info-book_publisher">Penerbit</label>
                    </div>
                    <div class="col-md-8">
                        <div class="info-book_publisher">-</div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4">
                        <label for="info-book_title">Judul Buku</label>
                    </div>
                    <div class="col-md-8">
                        <div class="info-book_title">-</div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label for="info-book_available_stock">Stok Tersedia</label>
                    </div>
                    <div class="col-md-8">
                        <div class="info-book_available_stock">0</div>
                    </div>
                </div>
            </div>
        </div>
        <hr>

        <x-adminlte-datatable id="last-stock-card-table" :heads="$lastStockCardDatatable['heads']" head-theme="dark" :config="$lastStockCardDatatable['config']"
        striped hoverable bordered compressed/>

        <x-slot name="footerSlot">
            <x-adminlte-button theme="default" label="Tutup" data-dismiss="modal"/>
        </x-slot>
    </x-adminlte-modal>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script type="text/javascript">
    var _bookId = null;
    $(document).ready(function() {
        $(".btn-save").click((e)=> {
            e.preventDefault()
            // return;

            $(this).helperForm('click',{
                url: '/books/create',
                data: $('#book-form').serializeArray(),
                confirm: true,
                confirm_type: 'warning', // warning, error, info
                // confirm_title: 'Apakah bla bla?',
                // confirm_message: 'bla bla bla bla',
                success : function(res) {
                    $('.modal').modal('hide');
                    setTimeout(()=>{
                        Toast.fire({
                            icon: 'success',
                            title: 'Data berhasil disimpan'
                        });
                    }, 1000);
                }
            });
        });

        
        $(".btn-update").click((e)=> {
            e.preventDefault()
            // return;

            $(this).helperForm('click',{
                url: '/books/update',
                data: $('#book-form').serializeArray(),
                confirm: true,
                confirm_type: 'warning', // warning, error, info
                // confirm_title: 'Apakah bla bla?',
                // confirm_message: 'bla bla bla bla',
                success : function(res) {
                    $('.modal').modal('hide');
                    setTimeout(()=>{
                        Toast.fire({
                            icon: 'success',
                            title: 'Data berhasil diubah'
                        });
                    }, 1000);
                }
            });
        });
        
        $(document).on('click', '.btn-create-book', (e)=>{
            $('#modal-book').find('.modal-title').text('Tambah Buku');
            $('.btn-save').show();
            $('.btn-update').hide();
            clearForm();
        })

        function clearForm() {
            $('#book-form')[0].reset();
            $('#book-form').find('#publisher_id').val('').change();
        }

        $(document).on('click', '.btn-edit-book', (e)=>{
            e.preventDefault();
            var _this = e.target;
            var bookId = _this.dataset.id;

            $.ajax({
                url: '/books/get-book-by-id',
                type: 'GET',
                data: {book_id:bookId},
                beforeSend: function (xhr) {

                },
                success: function (data) {
                    console.log(data)
                    $('#book-form').find('#id').val(data.id);
                    $('#book-form').find('#code').val(data.code);
                    $('#book-form').find('#title').val(data.title);
                    $('#book-form').find('#isbn_code').val(data.isbn_code);
                    $('#book-form').find('#writer').val(data.writer);
                    $('#book-form').find('#publisher_id').val(data.publisher_id).change();
                    $('#book-form').find('#base_price').val(data.base_price);
                    $('#book-form').find('#available_stock').val(data.available_stock);
                }
            });

            $('#modal-book').find('.modal-title').text('Ubah Data Buku');
            $('.btn-save').hide();
            $('.btn-update').show();
        })

        // ______modal form adjustment

        
        
        $(document).on('change', '.calculate', (e)=>{
            let adjustmentType = $('#adjustment_type').val();
            let availableStock = $('#available_stock').val();
            let adjustmentValue = $('#adjustment_value').val();
            let newStock = availableStock;

            if (!adjustmentType || adjustmentType == "") {
                newStock = availableStock;
            } else {
                if (adjustmentType == 'stock_in') {
                    newStock = parseInt(newStock) + parseInt(adjustmentValue);
                } else {
                    newStock = parseInt(newStock) - parseInt(adjustmentValue);
                }
            }

            $('#final_stock').val(newStock);
        })

        $(document).on('click', '.btn-adjustment-stock', (e)=>{
            e.preventDefault();
            var _this = e.target;
            var bookId = _this.dataset.id;

            clearFormAdjustment();

            $('#book_id').val(bookId);

            $.ajax({
                url: '/books/get-book-by-id',
                type: 'GET',
                data: {book_id:bookId},
                beforeSend: function (xhr) {

                },
                success: function (data) {
                    console.log(data)
                    $('#book_id').html(data.id);
                    $('.info-book_code').html(data.code);
                    $('.info-book_title').html(data.title);
                    $('.info-book_publisher').html(data.publisher_name);
                    $('.info-book_available_stock').html(data.available_stock);
                    // on form
                    $('#available_stock').val(data.available_stock);
                    // $('#adjustment_value').val(0);
                    $('#final_stock').val(data.available_stock);
                }
            });
        })

        $(document).on('click', '.btn-save-adjustment', (e) => {
            e.preventDefault();

            // console.log($('#stock-adjustment-form').serializeArray());

            $(this).helperForm('click',{
                url: '/books/create-stock-adjustment',
                data: $('#stock-adjustment-form').serializeArray(),
                confirm: true,
                confirm_type: 'warning', // warning, error, info
                // confirm_title: 'Apakah bla bla?',
                // confirm_message: 'bla bla bla bla',
                success : function(res) {
                    $('.modal').modal('hide');
                    setTimeout(()=>{
                        Toast.fire({
                            icon: 'success',
                            title: 'Penyesuaian Stok Berhasil Disimpan'
                        });
                    }, 1000);
                    clearFormAdjustment();
                }
            });
        })

        function clearFormAdjustment()
        {
            $('#stock-adjustment-form')[0].reset();
            $('#stock-adjustment-form').find('#adjustment_type').val('').change();
        }


        // ---------------- stock card
        $(document).on('click', '.btn-last-stock-card', (e)=>{
            e.preventDefault();
            var _this = e.target;
            _bookId = _this.dataset.id;

            $.ajax({
                url: '/books/get-book-by-id',
                type: 'GET',
                data: {book_id:_bookId},
                beforeSend: function (xhr) {

                },
                success: function (data) {
                    console.log(data)
                    $('#book_id').html(data.id);
                    $('.info-book_code').html(data.code);
                    $('.info-book_title').html(data.title);
                    $('.info-book_publisher').html(data.publisher_name);
                    $('.info-book_available_stock').html(data.available_stock);
                }
            });

            var testURL = `/books/get-data-last-stock-card?book_id=${_bookId}`;
            $('#last-stock-card-table').DataTable().ajax.url(testURL).load();
        })
    })
</script>
@stop