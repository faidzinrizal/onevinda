@extends('adminlte::page')

@section('title', 'Penjualan')

@section('content_header')
    <h1>Informasi Penjualan</h1>
    <div class="btn-group">
        <a href="/order/create" class="btn btn-success btn-sm">Tambah Penjualan</a>
    </div>
@stop

@section('content')
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

    <x-adminlte-datatable id="sale-table" :heads="$saleDatatable['heads']" head-theme="dark" :config="$saleDatatable['config']"
    striped hoverable bordered compressed/>

    <x-adminlte-modal id="modal-payment" title="Pembayaran" size="lg">
        <form id="payment-form">
            <div class="row">
                @csrf
                <input type="hidden" id="id" name="id" value=""> 
                <x-adminlte-textarea 
                    name="payment_note" 
                    label="Catatan Pembayaran" 
                    placeholder="Bukti transfer atau catatan lain"
                    fgroup-class="col-md-12" 
                    rows=5
                    disable-feedback="true"
                    />
            </div>
        </form>

        <x-slot name="footerSlot">
            <x-adminlte-button class="mr-auto btn-save-payment" theme="success" label="Simpan Pembayaran"/>
            <x-adminlte-button theme="default" label="Batal" data-dismiss="modal"/>
        </x-slot>
    </x-adminlte-modal>
@stop


@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script type="text/javascript">
// $(document).on('click', '.btn-pay', (e)=>{
//     var _this = e.target;
//     _id = _this.dataset.id;
//     $(this).helperForm('click',{
//         url: '/sales/pay',
//         data: {sale_id: _id, _token:$('meta[name="csrf-token"]').attr('content')},
//         confirm: true,
//         confirm_type: 'warning', // warning, error, info
//         success : function(res) {
//             console.log(res);
//             Toast.fire({
//                 icon: 'success',
//                 title: 'Pembayaran berhasil disimpan'
//             });
            
//             var url = `/sales/get-data`;
//             $('#sale-table').DataTable().ajax.url(url).load();
//         }
//     });
// });

$(document).on('click', '.btn-cancel-order', (e)=>{
    var _this = e.target;
    _id = _this.dataset.id;
    $(this).helperForm('click',{
        url: '/sales/cancel-order',
        data: {sale_id: _id, _token:$('meta[name="csrf-token"]').attr('content')},
        confirm: true,
        confirm_type: 'warning', // warning, error, info
        success : function(res) {
            console.log(res);
            Toast.fire({
                icon: 'success',
                title: 'Transaksi berhasil dibatalkan'
            });
            
            var url = `/sales/get-data`;
            $('#sale-table').DataTable().ajax.url(url).load();
        }
    });
});

$(document).on('click', '.btn-payment', (e)=>{
    e.preventDefault();
    var _this = e.target;
    var saleId = _this.dataset.id;

    $.ajax({
        url: '/sales/get-sale-by-id',
        type: 'GET',
        data: {sale_id:saleId},
        beforeSend: function (xhr) {

        },
        success: function (data) {
            console.log(data)
            $('#payment-form').find('#id').val(data.id);
            $('#payment-form').find('#payment_note').val(data.payment_note);
        }
    });
})

$(document).on('click', '.btn-save-payment', (e)=>{
    e.preventDefault()
    // return;

    $(this).helperForm('click',{
        url: '/sales/pay',
        data: $('#payment-form').serializeArray(),
        confirm: true,
        confirm_type: 'warning', // warning, error, info
        confirm_title: 'Apakah anda yakin akan menyimpan pembayaran ini?',
        // confirm_message: 'bla bla bla bla',
        success : function(res) {
            $('.modal').modal('hide');
            setTimeout(()=>{
                Toast.fire({
                    icon: 'success',
                    title: 'Pembayaran berhasil'
                });
            }, 1000);
        }
    });
});
</script>
@stop