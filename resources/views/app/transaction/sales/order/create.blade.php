@extends('adminlte::page')

@section('title', 'Tambah Penjualan')

@section('content_header')
    <h1>Tambah Penjualan</h1>
@stop

@section('content')
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

    <!-- create form -->
    <form method="post" id="order-form">
        @csrf
        <div class="row">
            <div class="col-md-6">
                <x-adminlte-select2 name="customer_id"
                    label="Reseller"
                    placeholder="Reseller"
                    fgroup-class="col-md-12"
                    disable-feedback="true">
                    <x-adminlte-options :options=$customerList empty-option="--Pilih--"/>
                </x-adminlte-select2>
                <x-adminlte-input 
                    name="date" 
                    label="Tanggal" 
                    placeholder="dd-mm-yyyy"
                    fgroup-class="col-md-12"
                    value="{{ date('d-m-Y H:i:s') }}" 
                    disable-feedback="true"/>
            </div>
            <div class="col-md-6">
                <x-adminlte-select2 name="sale_type"
                    label="Cara Bayar"
                    placeholder="Cara Bayar"
                    fgroup-class="col-md-12"
                    disable-feedback="true">
                    <x-adminlte-options :options=$saleTypeList empty-option="--Pilih--"/>
                </x-adminlte-select2>
                <x-adminlte-input 
                    name="due_date" 
                    label="Jatuh Tempo" 
                    placeholder="dd-mm-yyyy"
                    fgroup-class="col-md-12" 
                    disable-feedback="true"/>
            </div>
        </div>
        <div class="row">
        
        <x-adminlte-datatable id="order-table" :heads="$orderDatatable['heads']" head-theme="dark" :config="$orderDatatable['config']" striped hoverable bordered compressed />
        <table style="width: 100%;" class="table table-bordered table-hover table-striped table-sm no-footer">
            <tr>
                <td colspan=2 width="55%">
                    <x-adminlte-select2 name="book_id"
                        hasLabel=false
                        fgroup-class="col-md-12"
                        disable-feedback="true">
                        <x-adminlte-options :options=$bookList empty-option="--Pilih--"/>
                    </x-adminlte-select2>
                </td>
                <td width="10%">
                    <x-adminlte-input 
                        name="quantity" 
                        hasLabel=false 
                        placeholder="Jumlah"
                        fgroup-class="col-md-12" 
                        disable-feedback="true"/>
                </td>
                <td width="10%"><div class="base_price"></div></td>
                <td width="10%">
                    <x-adminlte-input 
                        name="discount" 
                        hasLabel=false 
                        placeholder="Diskon (%)"
                        fgroup-class="col-md-12" 
                        value="30"
                        disable-feedback="true"/>
                </td>
                <td width="10%"><div class="subtotal"></div></td>
                <td width="50%"><x-adminlte-button class="btn-sm btn-add" theme="info" label="Tambah"/></td>
            </tr>
        </table>
            
        </div>
        <div class="row">
            <div class="col-md-8"></div>
            <div class="col-md-4">
                <x-adminlte-input 
                    name="subtotal" 
                    label="Sub Total"
                    class="calculate"
                    placeholder="Sub Total"
                    value="0"
                    fgroup-class="col-md-12" 
                    disable-feedback="true"/>
                    <x-adminlte-input 
                    name="shipping_cost" 
                    label="Ongkos Kirim" 
                    class="calculate"
                    placeholder="Ongkos Kirim"
                    fgroup-class="col-md-12" 
                    value="0"
                    disable-feedback="true"/>
                <x-adminlte-input 
                    name="total" 
                    label="Total" 
                    placeholder="Total"
                    value="0"
                    fgroup-class="col-md-12" 
                    disable-feedback="true"/>
            </div>

        </div>
        <div class="row">
            <div class="col-md-12 text-right">
                <x-adminlte-button class="btn-save" style="margin-right: 10px;" theme="success" label="Simpan"/>
            </div>
        </div>
    </form>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script type="text/javascript">
$("#due_date").datepicker({
    format: "dd-mm-yyyy",
    startDate: Date(),
    autoclose: true
});
var csrf = $('meta[name="csrf-token"]').attr('content');
$(".btn-add").click((e) => {
    e.preventDefault();
    $.ajax({
        url: '/order/add-book',
        type: 'POST',
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
            book_id: $("#book_id").val(),
            quantity: $("#quantity").val(),
            discount: $("#discount").val(),
            
        },
        beforeSend: function (xhr) {

        },
        success: function (data) {
            console.log(data);
            clearDetailForm();

            $("#subtotal").val(data.subtotal).trigger('change');
            // $("#total").val(data.subtotal + $('#shipping_cost').val());
        },
        error: function (xhr, ajaxOptions, thrownError) {
            response = xhr.responseJSON;

            Toast.fire({
                icon: 'alert',
                title: response.message
            });
        }
    });
});

function clearForm() {
    $('#customer_id').val('').trigger('change');
    $('#sale_type').val('').trigger('change');
    $('#due_date').val('');
    $('#subtotal').val('0');
    $('#shipping_cost').val('0');
    $('#total').val('0');
    clearDetailForm();
}

function clearDetailForm() {
    var url = `/order/get-data-order`;
    $('#order-table').DataTable().ajax.url(url).load();
    $('#book_id').val('').trigger('change');
    $('#quantity').val('');
    $('#discount').val('30');
}

$(".calculate").change(() => {
    $("#total").val(parseInt($("#subtotal").val()) + parseInt($('#shipping_cost').val()));
});

$('.btn-save').click((e)=>{
    e.preventDefault();
    console.log($('#order-form').serializeArray());
    $(this).helperForm('click',{
        url: '/order/save',
        data: $('#order-form').serializeArray(),
        confirm: true,
        confirm_type: 'warning', // warning, error, info
        // confirm_title: 'Apakah bla bla?',
        // confirm_message: 'bla bla bla bla',
        success : function(res) {
            console.log(res);
            Toast.fire({
                icon: 'success',
                title: 'Penjualan Berhasil Disimpan'
            });
            clearForm();
        }
    });
});
</script>
@stop