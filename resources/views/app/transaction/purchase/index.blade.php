@extends('adminlte::page')

@section('title', 'Pembelian')

@section('content_header')
    <h1>Informasi Pembelian</h1>
    <div class="btn-group">
        <a href="/book-receipt/create" class="btn btn-success btn-sm">Tambah Pembelian</a>
    </div>
@stop

@section('content')
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

    <x-adminlte-datatable id="purchase-table" :heads="$purchaseDatatable['heads']" head-theme="dark" :config="$purchaseDatatable['config']"
    striped hoverable bordered compressed/>

@stop


@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script type="text/javascript">

</script>
@stop