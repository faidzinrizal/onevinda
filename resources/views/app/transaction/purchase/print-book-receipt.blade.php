<?php
use App\Http\Helpers\BaseHelper;
?>
<style type="text/css">
    body{
        font-family: 'Roboto Condensed', sans-serif;
    }
    .m-0{
        margin: 0px;
    }
    .p-0{
        padding: 0px;
    }
    .pt-5{
        padding-top:5px;
    }
    .mt-10{
        margin-top:10px;
    }
    .text-center{
        text-align:center !important;
    }
    .w-100{
        width: 100%;
    }
    .w-50{
        width:50%;   
    }
    .w-55{
        width:55%;   
    }
    .w-45{
        width:45%;   
    }
    .w-85{
        width:85%;   
    }
    .w-15{
        width:15%;   
    }
    .logo img{
        width:45px;
        height:45px;
        padding-top:30px;
    }
    .logo span{
        margin-left:8px;
        top:19px;
        position: absolute;
        font-weight: bold;
        font-size:25px;
    }
    .gray-color{
        color:#5D5D5D;
    }
    .text-bold{
        font-weight: bold;
    }
    .border{
        border:1px solid black;
    }
    /* table tr,th,td{
        border: 1px solid #d2d2d2;
        border-collapse:collapse;
        padding:7px 8px;
    }
    table tr th{
        background: #F4F4F4;
        font-size:15px;
    }
    table tr td{
        font-size:13px;
    } */
    table{
        border-collapse:collapse;
    }
    .box-text p{
        line-height:10px;
    }
    .float-left{
        float:left;
    }
    .total-part{
        font-size:16px;
        line-height:12px;
    }
    .total-right p{
        padding-right:20px;
    }
</style>
<body>
<div class="head-title">
    <table class="w-100">
        <tr>
            <td><img src="image/tb_purwa.png" alt="" srcset=""></td>
            <td style="text-align: center;">
                <div class="text-center m-0 p-0">
                    <h2><u>Toko Buku Purwa</u></h2>
                    <i>"Toko Buku dan Distributor"</i><br>
                    <small>Jl. Galaksi VI No. 89 Margahayu Raya - Bandung. Mobile. 085267573001</small>
                </div>
                <h3>PEMBELIAN BUKU</h3>
            </td>
        </tr>
    </table>
</div>

<div class="add-detail mt-10">
    <div class="w-50 float-left mt-10">
        
    </div>
    <div class="w-50 float-left mt-10">
        <p class="m-0 pt-5 text-bold w-100">No. Transaksi : <span class="gray-color">{{ $dataPurchase['purchase']->transaction_no }}</span></p>
        <p class="m-0 pt-5 text-bold w-100">Tanggal Pembelian : <span class="gray-color">{{ date('d-m-Y', strtotime($dataPurchase['purchase']->date)) }}</span></p>
    </div>
    <div style="clear: both;"></div>
</div>

<div class="table-section bill-tbl w-100 mt-10">
    <table class="table w-100 mt-10">
        <tr style="border-top: 1px solid;">
            <th style="width: 2%">NO</th>
            <th style="width: 10%">KODE</th>
            <th style="width: 30%">JUDUL BUKU</th>
            <th style="width: 20%; text-align: center;">PENULIS</th>
            <th style="width: 10%; text-align: center;">PENERBIT</th>
            <th style="width: 9%; text-align: right;">HARGA</th>
            <th style="width: 9%; text-align: center;">DISK</th>
            <th style="width: 5%; text-align: center;">QTY</th>
            <th style="width: 10%; text-align: right;">JUMLAH</th>
        </tr>

        <?php $subtotal = 0; ?>
        @foreach($dataPurchase['purchaseDetail'] as $key=>$purchaseDetail)
            <tr>
                <td style="text-align: center;">{{ $key + 1 }}</td>
                <td style="text-align: center;">{{ $purchaseDetail->book_code }}</td>
                <td>{{ $purchaseDetail->book_title }}</td>
                <td>{{ $purchaseDetail->book_writer }}</td>
                <td>{{ $purchaseDetail->book_publisher_name }}</td>
                <td style="text-align: right;">{{ BaseHelper::rupiahFormat($purchaseDetail->price) }}</td>
                <td style="text-align: center;">{{ BaseHelper::numberFormat($purchaseDetail->discount) }} %</td>
                <td style="text-align: center;">{{ BaseHelper::numberFormat($purchaseDetail->quantity) }}</td>
                <td style="text-align: right;">{{ BaseHelper::rupiahFormat($purchaseDetail->total_price) }}</td>
            </tr>
            <?php
            $subtotal += $purchaseDetail->total_price;
            ?>
        @endforeach
        <tr style="border-top: 1px solid;">
            <td colspan="4" style="vertical-align: top;">
                @if($dataPurchase['purchase']->purchase_note)
                <br>
                Catatan Pembayaran : <br>
                {{$dataPurchase['purchase']->purchase_note}}
                @endif
            </td>
            <td colspan="5">
                <div class="total-part">
                    <div class="total-left w-55 float-left" align="right">
                        <p>Sub Total</p>
                        <p>Ongkir</p>
                        <p>Total</p>
                        <p>Jatuh Tempo</p>
                        <p>Status</p>
                    </div>
                    <div class="total-right w-45 float-left text-bold" align="right">
                        <p>{{ BaseHelper::rupiahFormat($subtotal) }}</p>
                        <p>{{ BaseHelper::rupiahFormat($dataPurchase['purchase']->shipping_cost) }}</p>
                        <p>{{ BaseHelper::rupiahFormat($subtotal + $dataPurchase['purchase']->shipping_cost) }}</p>
                        <p>{{ $dataPurchase['purchase']->due_date ? date('d-m-Y', strtotime($dataPurchase['purchase']->due_date)) : '-' }}</p>
                        <p>{{ $dataPurchase['purchase']->status_name }}</p>
                    </div>
                    <div style="clear: both;"></div>
                </div> 
            </td>
        </tr>
    </table>
</div>